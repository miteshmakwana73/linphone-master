package org.linphone.assistant;
/*
LoginFragment.java
Copyright (C) 2015  Belledonne Communications, Grenoble, France

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/
import org.json.JSONArray;
import org.json.JSONObject;
import org.linphone.LinphoneActivity;
import org.linphone.LinphonePreferences;
import org.linphone.core.LinphoneAddress.TransportType;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.linphone.R;
import org.linphone.jsonurl.Config;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Sylvain Berfini
 */
public class LoginFragment extends Fragment implements OnClickListener, TextWatcher {
    private Context mContext=getActivity();

    private TextView tvTitle;//,tvDescription, tvUsername,tvPassword,tvDomain;
	private EditText login, password, displayName, domain;
	private RadioGroup transports;
	private RadioButton transportsUdp,transportsTcp,transportsTls;
	private Button apply;

    String URL_LOGIN = Config.LOGIN_URL;

    @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.assistant_login, container, false);

		tvTitle = (TextView) view.findViewById(R.id.tvtitle);
		/*tvDescription = (TextView) view.findViewById(R.id.tvdescription);
		tvUsername = (TextView) view.findViewById(R.id.tvusername);
		tvPassword = (TextView) view.findViewById(R.id.tvpassword);
		tvDomain = (TextView) view.findViewById(R.id.tvdomain);*/
		transportsUdp = (RadioButton) view.findViewById(R.id.transport_udp);
		transportsTcp = (RadioButton) view.findViewById(R.id.transport_tcp);
		transportsTls = (RadioButton) view.findViewById(R.id.transport_tls);

		Shader myShader = new LinearGradient(
                100, 0, 0, 40,
                Color.parseColor("#59C6EB"), Color.parseColor("#73D99B"),
                Shader.TileMode.CLAMP );
        tvTitle.getPaint().setShader(myShader);
//        tvTitle.setText("Sip Call");

		login = (EditText) view.findViewById(R.id.assistant_username);
		login.addTextChangedListener(this);
		password = (EditText) view.findViewById(R.id.assistant_password);
		password.addTextChangedListener(this);
		displayName = (EditText) view.findViewById(R.id.assistant_display_name);
		domain = (EditText) view.findViewById(R.id.assistant_domain);
		domain.addTextChangedListener(this);
		transports = (RadioGroup) view.findViewById(R.id.assistant_transports);
		apply = (Button) view.findViewById(R.id.assistant_apply);
		apply.setEnabled(false);
		apply.setOnClickListener(this);

		/*Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "fonts/maven_pro_regular.ttf");
		Typeface tf2 = Typeface.createFromAsset(getActivity().getAssets(), "fonts/maven_pro_medium.ttf");
		tvTitle.setTypeface(tf);
		tvDescription.setTypeface(tf);
		tvUsername.setTypeface(tf2);
		tvPassword.setTypeface(tf2);
		tvDomain.setTypeface(tf2);
		transportsUdp.setTypeface(tf2);
		transportsTcp.setTypeface(tf2);
		transportsTls.setTypeface(tf2);
		apply.setTypeface(tf2);*/

        checkLogin();

        return view;
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		
		if (id == R.id.assistant_apply) {
			if (login.getText() == null || login.length() == 0 || password.getText() == null || password.length() == 0 || domain.getText() == null || domain.length() == 0) {
				Toast.makeText(getActivity(), getString(R.string.first_launch_no_login_password), Toast.LENGTH_LONG).show();
				return;
			}

			TransportType transport;
			if(transports.getCheckedRadioButtonId() == R.id.transport_udp){//TODO Select UDP registration
				transport = TransportType.LinphoneTransportUdp;
			} else {
				if(transports.getCheckedRadioButtonId() == R.id.transport_tcp){
					transport = TransportType.LinphoneTransportTcp;
				} else {
					transport = TransportType.LinphoneTransportTls;
				}
			}
			loginCheck(transport);
			//TODO Start calling the login interface
//			AssistantActivity.instance().genericLogIn(login.getText().toString(), password.getText().toString(), displayName.getText().toString(), domain.getText().toString(), transport);
		}
	}

    public void loginCheck(final TransportType transport) {
        // Creating string request with post method.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_LOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {

                        // Hiding the progress dialog after all task complete.
//                        progressBar.setVisibility(View.GONE);
                        try {
                            JSONObject jobj = new JSONObject(ServerResponse);

                            String status = jobj.getString("success");

                            String msg = jobj.getString("message");

                            if (status.equals("true")) {
                                //move to next page

                                JSONObject obj = new JSONObject(ServerResponse);

                                JSONObject jsonobject = obj.getJSONObject("data");

                                String currency = null,balance = null,token = null;

                                currency=jsonobject.getString("currency");
                                balance=jsonobject.getString("balance");
                                token=jsonobject.getString("token");
                                //Log.e("Data",ServerResponse);

                                /*for(int i=0; i < heroarray.length(); i++) {
                                    JSONObject jsonobject = heroarray.getJSONObject(i);
                                    currency=jsonobject.getString("currency");
                                    balance=jsonobject.getString("balance");
                                    token=jsonobject.getString("token");
                                }*/

                                SharedPreferences sharedPreferences = getActivity().getSharedPreferences("MyFile", 0);
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putString("currency",currency);
                                editor.putString("balance",balance);
                                editor.putString("token",token);
//                                editor.putInt("loginstatus",1);
                                editor.commit();

                                String username=login.getText().toString().trim();
                                String pass=password.getText().toString().trim();
                                String dis=displayName.getText().toString().trim();

                                //TODO Start calling the login interface
                                AssistantActivity.instance().genericLogIn(login.getText().toString(), password.getText().toString(), displayName.getText().toString(), domain.getText().toString(), transport);
                                Log.e("msg",msg);
                                showToast(msg);
                            }else {
                                showToast(msg);
                            }

                            //Toast.makeText(SignupActivity.this, ServerResponse, Toast.LENGTH_LONG).show();
                            Log.e("success",msg);
                        }catch (Exception e) {
                            e.printStackTrace();
                        }

                        // Showing response message coming from server.

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        // Hiding the progress dialog after all task complete.
//                        progressBar.setVisibility(View.GONE);

                        if( volleyError instanceof NoConnectionError) {
                            showToast("No connection available");
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            showToast("Oops. Timeout error!");
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            showToast("Server error!");
                        }
                        else
                        {
                            showToast("Something Wrong");
                        }
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("x-api-key", Config.DEFAULT_LOGIN_KEY);

                return headers;
            }
            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();

                String username=login.getText().toString().trim();
                String pass=password.getText().toString().trim();
                // Adding All values to Params.
                params.put("username", username);
                params.put("password", pass);

                return params;
            }

        };

        // Creating RequestQueue.
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());

        // Adding the StringRequest object into requestQueue.
        requestQueue.add(stringRequest);
    }

    private void showToast(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		apply.setEnabled(!login.getText().toString().isEmpty() && !password.getText().toString().isEmpty() && !domain.getText().toString().isEmpty());
	}

	@Override
	public void afterTextChanged(Editable s) {

	}

    private void checkLogin() {
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("MyFile", 0);
        String uname = sharedPreferences.getString("username","");
        if (uname.equalsIgnoreCase("")) {

        }
        else
        {
            Intent intent = new Intent(mContext, LinphoneActivity.class);
            startActivity(intent);
            getActivity().finish();
        }
    }
}
