package org.linphone;
/*
HistoryDetailFragment.java
Copyright (C) 2012  Belledonne Communications, Grenoble, France

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/
import org.linphone.core.CallDirection;
import org.linphone.core.LinphoneAddress;
import org.linphone.core.LinphoneCallLog;
import org.linphone.core.LinphoneCoreException;
import org.linphone.core.LinphoneCoreFactory;
import org.linphone.mediastream.Log;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import org.linphone.R;

import java.util.Arrays;

/**
 * @author Sylvain Berfini
 */
public class HistoryDetailFragment extends Fragment implements OnClickListener {
	private ImageView dialBack, chat, addToContacts, back;
	private View view;
	private ImageView contactPicture, callDirection;
	private TextView contactName, contactAddress, time, date;
	private String sipUri, displayName, pictureUri;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		sipUri = getArguments().getString("SipUri");
		displayName = getArguments().getString("DisplayName");
		pictureUri = getArguments().getString("PictureUri");
		String status = getArguments().getString("CallStatus");
		String callTime = getArguments().getString("CallTime");
		String callDate = getArguments().getString("CallDate");

		view = inflater.inflate(R.layout.history_detail, container, false);
		
		dialBack = (ImageView) view.findViewById(R.id.call);
		dialBack.setOnClickListener(this);

		back = (ImageView) view.findViewById(R.id.back);
		if(getResources().getBoolean(R.bool.isTablet)){
			back.setVisibility(View.INVISIBLE);
		} else {
			back.setOnClickListener(this);
		}
		
		chat = (ImageView) view.findViewById(R.id.chat);
		chat.setOnClickListener(this);
		if (getResources().getBoolean(R.bool.disable_chat))
			view.findViewById(R.id.chat).setVisibility(View.GONE);
		
		addToContacts = (ImageView) view.findViewById(R.id.add_contact);
		addToContacts.setOnClickListener(this);
		
		contactPicture = (ImageView) view.findViewById(R.id.contact_picture);
		
		contactName = (TextView) view.findViewById(R.id.contact_name);
		contactAddress = (TextView) view.findViewById(R.id.contact_address);
		
		callDirection = (ImageView) view.findViewById(R.id.direction);
		
		time = (TextView) view.findViewById(R.id.time);
		date = (TextView) view.findViewById(R.id.date);
		
		displayHistory(status, callTime, callDate);
		
		return view;
	}
	
	private void displayHistory(String status, String callTime, String callDate) {
		if (status.equals(getResources().getString(R.string.missed))) {
			callDirection.setImageResource(R.drawable.ic_call_status_missed_black_24dp);
		} else if (status.equals(getResources().getString(R.string.incoming))) {
			callDirection.setImageResource(R.drawable.ic_call_status_incoming_black_24dp);
		} else if (status.equals(getResources().getString(R.string.outgoing))) {
			callDirection.setImageResource(R.drawable.ic_call_status_outgoing_black_24dp);
		}
		
		time.setText(callTime == null ? "" : callTime);
		Long longDate = Long.parseLong(callDate);
		date.setText(LinphoneUtils.timestampToHumanDate(getActivity(),longDate,getString(R.string.history_detail_date_format)));

		LinphoneAddress lAddress = null;
		try {
			lAddress = LinphoneCoreFactory.instance().createLinphoneAddress(sipUri);
		} catch (LinphoneCoreException e) {
			Log.e(e);
		}

		if(lAddress != null) {
			contactAddress.setText(lAddress.asStringUriOnly());
			LinphoneContact contact = ContactsManager.getInstance().findContactFromAddress(lAddress);
			if (contact != null) {
				contactName.setText(contact.getFullName());
				LinphoneUtils.setImagePictureFromUri(view.getContext(),contactPicture,contact.getPhotoUri(),contact.getThumbnailUri());
				addToContacts.setVisibility(View.INVISIBLE);
			} else {
				contactName.setText(displayName == null ? LinphoneUtils.getAddressDisplayName(sipUri) : displayName);
				contactPicture.setImageResource(R.drawable.ic_avatar_background);
				addToContacts.setVisibility(View.VISIBLE);
			}
		} else {
			contactAddress.setText(sipUri);
			contactName.setText(displayName == null ? LinphoneUtils.getAddressDisplayName(sipUri) : displayName);
		}
	}
	
	public void changeDisplayedHistory(String sipUri, String displayName, String pictureUri, String status, String callTime, String callDate) {		
		if (displayName == null ) {
			displayName = LinphoneUtils.getUsernameFromAddress(sipUri);
		}

		this.sipUri = sipUri;
		this.displayName = displayName;
		this.pictureUri = pictureUri;
		displayHistory(status, callTime, callDate);
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		if (LinphoneActivity.isInstanciated()) {
			LinphoneActivity.instance().selectMenu(FragmentsAvailable.HISTORY_DETAIL);
			LinphoneActivity.instance().hideTabBar(false);
		}
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();

		if (id == R.id.back) {
			getFragmentManager().popBackStackImmediate();
		} if (id == R.id.call) {
			LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View layout = inflater.inflate(R.layout.popupdialog, null);
			final PopupWindow window = new PopupWindow(layout, ViewGroup.LayoutParams.WRAP_CONTENT,  ViewGroup.LayoutParams.WRAP_CONTENT);

			/*if(temp==1)
			{
				window.dismiss();
				temp=0;
				return;
			}*/
			window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
			window.setOutsideTouchable(true);
			window.showAtLocation(layout, Gravity.CENTER, 0, 0);
//			temp=1;
			//  window.showAtLocation(layout, 17, 100, 100);

			final TextView tvCall = (TextView) layout.findViewById(R.id.tvcall);
			final TextView tvVideo = (TextView) layout.findViewById(R.id.tvvideo);

			SharedPreferences sharedPreferences = getActivity().getSharedPreferences("MyFile", 0);
			final SharedPreferences.Editor editor = sharedPreferences.edit();

			tvCall.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					LinphonePreferences mPrefs;
					mPrefs = LinphonePreferences.instance();
					mPrefs.enableVideo(false);
					window.dismiss();
//					temp = 0;

					editor.putString("callvideostatus","1");
					editor.commit();

					LinphoneActivity.instance().setAddresGoToDialerAndCall(sipUri, displayName, pictureUri == null ? null : Uri.parse(pictureUri));
				}
			});

			tvVideo.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					LinphonePreferences mPrefs;
					mPrefs = LinphonePreferences.instance();
					mPrefs.enableVideo(true);

					window.dismiss();
//					temp = 0;

					editor.putString("callvideostatus","2");
					editor.commit();

					LinphoneActivity.instance().setAddresGoToDialerAndCall(sipUri, displayName, pictureUri == null ? null : Uri.parse(pictureUri));

				}
			});
//			LinphoneActivity.instance().setAddresGoToDialerAndCall(sipUri, displayName, pictureUri == null ? null : Uri.parse(pictureUri));
		} else if (id == R.id.chat) {
			LinphoneActivity.instance().displayChat(sipUri);
		} else if (id == R.id.add_contact) {
			String uri = sipUri;
			try {
				LinphoneAddress addr = LinphoneCoreFactory.instance().createLinphoneAddress(sipUri);
				uri = addr.asStringUriOnly();
			} catch (LinphoneCoreException e) {
				Log.e(e);
			}
			LinphoneActivity.instance().displayContactsForEdition(uri);
		}
	}
}
