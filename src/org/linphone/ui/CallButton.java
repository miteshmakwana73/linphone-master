/*
CallButton.java
Copyright (C) 2010  Belledonne Communications, Grenoble, France

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.linphone.ui;

import org.linphone.LinphoneActivity;
import org.linphone.LinphoneManager;
import org.linphone.LinphonePreferences;
import org.linphone.core.CallDirection;
import org.linphone.core.LinphoneCallLog;
import org.linphone.core.LinphoneCore;
import org.linphone.core.LinphoneCoreException;
import org.linphone.core.LinphoneProxyConfig;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import org.linphone.R;

/**
 * @author Guillaume Beraudo
 */
public class CallButton extends ImageView implements OnClickListener, AddressAware {

	private AddressText mAddress;
	int temp=0;
	public void setAddressWidget(AddressText a) { mAddress = a; }

	public void setExternalClickListener(OnClickListener e) { setOnClickListener(e); }
	public void resetClickListener() { setOnClickListener(this); }

	public CallButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		setOnClickListener(this);
	}

	public void onClick(View v) {//Click the call button to make a call
		try {
			if (!LinphoneManager.getInstance().acceptCallIfIncomingPending()) {
				if (mAddress.getText().length() > 0) { //dial number
					LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					View layout = inflater.inflate(R.layout.popupdialog, null);
					final PopupWindow window = new PopupWindow(layout, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

					if (temp == 1) {
						window.dismiss();
						temp = 0;
						return;
					}
					window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
					window.setOutsideTouchable(true);
					window.showAtLocation(layout, Gravity.CENTER, 0, 0);
//					temp=1;
					//  window.showAtLocation(layout, 17, 100, 100);

					final TextView tvCall = (TextView) layout.findViewById(R.id.tvcall);
					final TextView tvVideo = (TextView) layout.findViewById(R.id.tvvideo);

					SharedPreferences sharedPreferences = getContext().getSharedPreferences("MyFile", 0);
					final SharedPreferences.Editor editor = sharedPreferences.edit();

					tvCall.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							LinphonePreferences mPrefs;
							mPrefs = LinphonePreferences.instance();
							mPrefs.enableVideo(false);
							window.dismiss();
//							temp = 0;

							editor.putString("callvideostatus", "1");
							editor.commit();

							LinphoneManager.getInstance().newOutgoingCall(mAddress);

						}
					});

					tvVideo.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							LinphonePreferences mPrefs;
							mPrefs = LinphonePreferences.instance();
							mPrefs.enableVideo(true);

							window.dismiss();
//							temp = 0;

							editor.putString("callvideostatus", "2");
							editor.commit();

							LinphoneManager.getInstance().newOutgoingCall(mAddress);
						}
					});

//					LinphoneManager.getInstance().newOutgoingCall(mAddress);

				} else {
					if (LinphonePreferences.instance().isBisFeatureEnabled()) {//Put the last recorded call on the input board
						LinphoneCallLog[] logs = LinphoneManager.getLc().getCallLogs();
						LinphoneCallLog log = null;
						for (LinphoneCallLog l : logs) {
							if (l.getDirection() == CallDirection.Outgoing) {
								log = l;
								break;
							}
						}
						if (log == null) {
							return;
						}

						LinphoneProxyConfig lpc = LinphoneManager.getLc().getDefaultProxyConfig();
						if (lpc != null && log.getTo().getDomain().equals(lpc.getDomain())) {
							mAddress.setText(log.getTo().getUserName());
						} else {
							mAddress.setText(log.getTo().asStringUriOnly());
						}
						mAddress.setSelection(mAddress.getText().toString().length());
						mAddress.setDisplayedName(log.getTo().getDisplayName());
					}
				}
			}
		} catch (LinphoneCoreException e) {
			LinphoneManager.getInstance().terminateCall();
			onWrongDestinationAddress();
		}
		catch (NullPointerException e){
			Log.e("Null pointer",e.getMessage());
		}
	}
	
	protected void onWrongDestinationAddress() {
		Toast.makeText(getContext()
				,String.format(getResources().getString(R.string.warning_wrong_destination_address),mAddress.getText().toString())
				,Toast.LENGTH_LONG).show();
	}
}
