package com.quhwa.model.login;

import com.quhwa.bean.Result;
import com.quhwa.bean.ReturnResult;
import com.quhwa.bean.UserInfo;

/**
 * Login to the model layer interface
 *
 * @author lxz
 * @date March 23, 2017
 */
public interface ILoginModel {
	void loadLoginData(LoginOnLoadListener loginOnLoadListener, UserInfo userInfo);
	interface LoginOnLoadListener{
		void onComplete(Result result);//Data available
		void onCompleteFail();//Unable to get data
		void onNoNet();
	}
	
	void recoverWidgetShowStatus(RecoverOnloadListener recoverOnloadListener);
	interface RecoverOnloadListener{
		void onComplete(UserInfo userInfo);
	}

	void sendToken(SendTokenOnLoadListener sendTokenOnLoadListener, String token);
	interface SendTokenOnLoadListener{
		void onComplete(ReturnResult result);//Data available
		void onCompleteFail();//Unable to get data
		void onNoNet();
	}
}
