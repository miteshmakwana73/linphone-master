package com.quhwa.presenter;

import android.os.Handler;

import com.quhwa.bean.ReturnResult;
import com.quhwa.model.userinfo.IUserInfoModel;
import com.quhwa.model.userinfo.UserInfoModelImpl;
import com.quhwa.utils.Code;
import com.quhwa.utils.CommonUtil;
import com.quhwa.utils.MSG;
import com.quhwa.utils.MyLog;
import com.quhwa.utils.TimerUtils;
import com.quhwa.view.IUserInfoView;

public class UserInfoPresenter {
	private IUserInfoView iUserInfoView;
	private String Tag = "UserInfoPresenter";
	private IUserInfoModel userInfoModel = new UserInfoModelImpl();
	public UserInfoPresenter(IUserInfoView iUserInfoView) {
		super();
		this.iUserInfoView = iUserInfoView;
	}
	/**
	 * Detect sip registration status
	 */
	public void checkSipRegisterStatus(final Handler handler){
		TimerUtils.sendSipMsgTimer(null, handler, MSG.MSG_REGISTER_SIP_SERVER,3000,true);
	}
	/**
	 * Start testing
	 */
	public void startCheck() {
		MyLog.print(Tag, "Detect sip registration status", MyLog.PRINT_RED);
		//iUserInfoView.showSipRegisterStatus(PJSipService.instance.getSipRegisterStatus());
	}
	/**
	 * Stop detection
	 */
	public void stopCheck(){
		TimerUtils.stopToSend();
	}

	public void deleteToken(){
		if(userInfoModel != null && iUserInfoView != null){
			userInfoModel.deleteToken(new IUserInfoModel.OnDeleteTokenlistener() {
				@Override
				public void onCompelete(ReturnResult result) {
					if(result.getCode() == Code.RETURN_SUCCESS){
						MyLog.print(Tag,"Delete the token successfully, start deleting the local related data, and exit the login.",MyLog.PRINT_RED);
						iUserInfoView.deleteLocalData();
					}else{
						MyLog.print(Tag,"Deleting the token failed, starting to delete the local related data, exit the login code:"+result.getCode(),MyLog.PRINT_RED);
						iUserInfoView.deleteLocalData();
					}
				}

				@Override
				public void onCompleteFail() {
					MyLog.print(Tag,"The server is busy, deleting the token fails, starting to delete the local related data, and exiting the login.",MyLog.PRINT_RED);
					iUserInfoView.deleteLocalData();
				}

				@Override
				public void onNoNet() {
					MyLog.print(Tag,"Without the network, delete the token into a failure, start deleting the local related data, and exit the login.",MyLog.PRINT_RED);
					iUserInfoView.deleteLocalData();
				}
			}, CommonUtil.getToken());
		}

	}
}
