package com.quhwa.db;
/**
 * Preferences local table name and key name
 *
 * @author lxz
 * @date March 23, 2017
 */
public class Table {
	//1、Preferences table
	/**
	 * Status table (user account, sip account login status)
	 */
	public static final String TAB_STATUS = "tab_login_status";
	/**
	 * Login key value of the status table
	 */
	public static final String TAB_LOGIN_STATUS_KEY = "username_loginStatus";
	/**
	 * Key value of sip login status table
	 */
	public static final String TAB_SIP_LOGIN_STATUS_KEY = "sip_loginStatus";
	/**
	 * User table (used to save user information)
	 */
	public static final String TAB_USER = "tab_user";
	/**
	 * username
	 */
	public static final String TAB_USER_NAME_KEY = "username";
	/**
	 * password
	 */
	public static final String TAB_USER_PASSWORD_KEY = "password";
	/**
	 * sipId
	 */
	public static final String TAB_USER_PASSWORD_SIP_ID = "sipid";
	/**
	 * sipPassword
	 */
	public static final String TAB_USER_PASSWORD_SIP_PASSWORD = "sippassword";
	/**
	 * sessionKey
	 */
	public static final String TAB_USER_SESSION_KEY = "sessionKey";
	/**
	 * User's userId
	 */
	public static final String TAB_USER_USERID_KEY = "userId";

	/**
	 * Incoming call sipId
	 */
	public static final String TAB_USER_PASSWORD_INCOMING_SIP_ID_key = "incoming_sipid";
	
	/**
	 * Guest password
	 */
	public static final String TAB_USER_VISITOR_PASSWORD_KEY = "visitor_password";
	/**
	 * Aurora push token
	 */
	public static final String TAB_USER_JPUSH_TOKEN_KEY = "token";
	
	/**
	 * Call type table
	 */
	public static final String TAB_CALL_TYPE = "tab_call_type";

	/**
	 * Message list
	 */
	public static final String TAB_MSG = "tab_msg";
	/**
	 * Number of messages
	 */
	public static final String TAB_MSG_COUNT_KEY = "msg_count";

	
	//2、Sqlite database
	/**
	 * Device table name
	 */
	public static final String DEVICELIST_TABLE_NAME = "deviceList";
	//Device list fields
	/**Device list id field*/
	public static final String DEVICELIST_COLUMN_ID = "id";
	/**Device list username field*/
	public static final String DEVICELIST_COLUMN_USERNAME = "username";
	/**Device list mac field*/
	public static final String DEVICELIST_COLUMN_MAC = "mac";
	/**Device list sipId field*/
	public static final String DEVICELIST_COLUMN_SIPID = "sipId";
	/**Device list roomNum field*/
	public static final String DEVICELIST_COLUMN_ROOMNUM = "roomNum";
	/**Device list extensionNum field*/
	public static final String DEVICELIST_COLUMN_EXTENSIONNUM = "extensionNum";
	/**Device List Blocking Call Status Field*/
	public static final String DEVICELIST_COLUMN_INCOMING_SHIELD_STATUS = "shieldStatus";
	/**Device name field*/
	public static final String DEVICELIST_COLUMN_DEVICE_NAME = "deviceName";
	/**Device alias field*/
	public static final String DEVICELIST_COLUMN_DEVICE_ALIAS = "deviceAlias";
	/**Door machine string*/
	public static final String DEVICELIST_COLUMN_DOOR_DEVICE_STR = "doorDeviceStr";
	/**device ID*/
	public static final String DEVICELIST_COLUMN_DEVICECODE = "deviceCode";

	/**
	 * Create a device list of sql
	 */
	public static final String CREATE_DEVICE_TABLE_SQL = 
	"CREATE TABLE IF NOT EXISTS "+DEVICELIST_TABLE_NAME+"("
	+ DEVICELIST_COLUMN_ID+" INTEGER,"
	+ DEVICELIST_COLUMN_USERNAME+" VARCHAR(50),"
	+ DEVICELIST_COLUMN_MAC+" VARCHAR(50),"
	+ DEVICELIST_COLUMN_SIPID+" VARCHAR(50),"
	+ DEVICELIST_COLUMN_ROOMNUM+" VARCHAR(50),"
	+ DEVICELIST_COLUMN_INCOMING_SHIELD_STATUS+" Integer,"
	+ DEVICELIST_COLUMN_DEVICE_NAME+" VARCHAR(50),"
	+ DEVICELIST_COLUMN_EXTENSIONNUM+" VARCHAR(50),"
	+ DEVICELIST_COLUMN_DEVICECODE+" VARCHAR(50),"
	+ DEVICELIST_COLUMN_DEVICE_ALIAS+" VARCHAR(50),"
	+ DEVICELIST_COLUMN_DOOR_DEVICE_STR+" VARCHAR(50))";
	
	/**
	 * Increase the shieldStatus column sql
	 */
	public static final String ADD_SHIELD_STATUS_COLUMN = 
	"ALTER TABLE "+DEVICELIST_TABLE_NAME+" ADD "+DEVICELIST_COLUMN_INCOMING_SHIELD_STATUS+" INTEGER DEFAULT 1";
	
	/**
	 * Increase deviceName column sql
	 */
	public static final String ADD_DEVICE_NAME_COLUMN = 
			"ALTER TABLE "+DEVICELIST_TABLE_NAME+" ADD "+DEVICELIST_COLUMN_DEVICE_NAME+" VARCHAR(50)";
	
	/**
	 * Increase the doorDeviceStr column sql
	 */
	public static final String ADD_DOOR_DEVICE_STR_COLUMN = 
			"ALTER TABLE "+DEVICELIST_TABLE_NAME+" ADD "+DEVICELIST_COLUMN_DOOR_DEVICE_STR+" VARCHAR(50)";
	/**
	 * Increase deviceCode column sql
	 */
	public static final String ADD_DEVICECODE_COLUMN =
			"ALTER TABLE "+DEVICELIST_TABLE_NAME+" ADD "+DEVICELIST_COLUMN_DEVICECODE+" VARCHAR(50)";
	/**
	 * Increase deviceCode column sql
	 */
	public static final String ADD_DEVICE_ALIAS_COLUMN =
			"ALTER TABLE "+DEVICELIST_TABLE_NAME+" ADD "+DEVICELIST_COLUMN_DEVICE_ALIAS+" VARCHAR(50)";

}
