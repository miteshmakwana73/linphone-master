package com.quhwa.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.quhwa.bean.BoundResult;
import com.quhwa.bean.BoundResult.Device;
import com.quhwa.utils.MyLog;
import com.quhwa.utils.MySharedPreferenceManager;
import java.util.ArrayList;
import java.util.List;

/**
 * Database management class
 *
 * @author lxz
 * @date April 13, 2017
 */
public class DBManager {
	private static String Tag = "DBManager";
	/*--------------------------------------Device list operation---------------------------------------------*/
	/**
	 * Insert a piece of data into the device list
	 * @param context
	 * @param device
	 * @return true：Inserted successfully    false：Insert failed
	 */
	public static void insertDeviceList(Context context, BoundResult.Device device){
		MyLog.print(Tag, "Device save number:"+device.toString(), MyLog.PRINT_RED);
		SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
		ContentValues cv = new ContentValues();
		//cv.put(Table.DEVICELIST_COLUMN_MAC, device.getMac());
		cv.put(Table.DEVICELIST_COLUMN_ID,device.getDeviceId());
		cv.put(Table.DEVICELIST_COLUMN_USERNAME, device.getUsername());
		cv.put(Table.DEVICELIST_COLUMN_SIPID, device.getSipid());
		cv.put(Table.DEVICELIST_COLUMN_ROOMNUM, device.getRoomNo());
		//cv.put(Table.DEVICELIST_COLUMN_EXTENSIONNUM, device.getExtensionNum());
		cv.put(Table.DEVICELIST_COLUMN_INCOMING_SHIELD_STATUS,device.getShieldStatus());
		cv.put(Table.DEVICELIST_COLUMN_DEVICE_NAME,device.getDeviceName());
		cv.put(Table.DEVICELIST_COLUMN_DEVICE_ALIAS,device.getDeviceAlias());
		cv.put(Table.DEVICELIST_COLUMN_DEVICECODE,device.getDeviceCode());
		cv.put(Table.DEVICELIST_COLUMN_DOOR_DEVICE_STR,device.getUnitDoorNo());
		if(db != null){
			long row = db.insert(Table.DEVICELIST_TABLE_NAME, null, cv);
			db.close();
			if(row >= 0){
				MyLog.print(Tag, "Data insertion succeeded", MyLog.PRINT_RED);
			}else{
				MyLog.print(Tag, "Data insertion failed", MyLog.PRINT_RED);
			}
		}
	}
	/**
	 * Update sipId and Mac address based on room number
	 * @param context
	 * @param device
	 * @return true：update completed    false：Update failed
	 */
	public static boolean updateMacAndSipIdByRoomNum(Context context,Device device){
		SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
		if(db != null){
			ContentValues cv = new ContentValues();
			cv.put(Table.DEVICELIST_COLUMN_MAC, device.getDeviceMac());
			cv.put(Table.DEVICELIST_COLUMN_SIPID, device.getSipid());
			int row = db.update(Table.DEVICELIST_TABLE_NAME, cv, Table.DEVICELIST_COLUMN_ROOMNUM+"=?",new String[]{device.getRoomNo()});
			db.close();
			if(row > 0){
				MyLog.print(Tag, "Data update succeeded", MyLog.PRINT_RED);
				return true;
			}else{
				MyLog.print(Tag, "Data update failed", MyLog.PRINT_RED);
				return false;
			}
		}
		return false;
	}
	/**
	 * Update shieldStatus based on room number
	 * @param context
	 * @param device
	 * @return true：update completed    false：Update failed
	 */
	public static boolean updateShieldStatusByRoomNum(Context context,Device device){
		SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
		if(db != null){
			ContentValues cv = new ContentValues();
			cv.put(Table.DEVICELIST_COLUMN_INCOMING_SHIELD_STATUS, device.getShieldStatus());
			int row = db.update(Table.DEVICELIST_TABLE_NAME, cv, Table.DEVICELIST_COLUMN_ROOMNUM+"=?",new String[]{device.getRoomNo()});
			db.close();
			if(row > 0){
				MyLog.print(Tag, "Data update succeeded", MyLog.PRINT_RED);
				return true;
			}else{
				MyLog.print(Tag, "Data update failed", MyLog.PRINT_RED);
				return false;
			}
		}
		return false;
	}

	/**
	 * Inquire key = value，Whether this data exists in the database
	 * @param context
	 * @param key
	 * @param value
	 * @return true：presence  false does not exist
	 */
	public static boolean getQueryByWhere(Context context,String key,String value){
		SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
		List<Device> devices = new ArrayList<Device>();
		if(db != null){
			Cursor cursor = db.query(Table.DEVICELIST_TABLE_NAME, null, key+"=?", new String[]{value}, null, null, null);
			while (cursor.moveToNext()) {
				Device device = new Device();
				device.setDeviceMac(cursor.getString(cursor.getColumnIndex(Table.DEVICELIST_COLUMN_MAC)));
				device.setUsername(cursor.getString(cursor.getColumnIndex(Table.DEVICELIST_COLUMN_USERNAME)));
				device.setSipid(cursor.getString(cursor.getColumnIndex(Table.DEVICELIST_COLUMN_SIPID)));
				device.setRoomNo(cursor.getString(cursor.getColumnIndex(Table.DEVICELIST_COLUMN_ROOMNUM)));
				//device.setUnitDoorNo(cursor.getString(cursor.getColumnIndex(Table.DEVICELIST_COLUMN_EXTENSIONNUM)));
				device.setDeviceName(cursor.getString(cursor.getColumnIndex(Table.DEVICELIST_COLUMN_DEVICE_NAME)));
				device.setDeviceAlias(cursor.getString(cursor.getColumnIndex(Table.DEVICELIST_COLUMN_DEVICE_ALIAS)));
				device.setShieldStatus(cursor.getInt(cursor.getColumnIndex(Table.DEVICELIST_COLUMN_INCOMING_SHIELD_STATUS)));
				device.setUnitDoorNo(cursor.getString(cursor.getColumnIndex(Table.DEVICELIST_COLUMN_DOOR_DEVICE_STR)));
				devices.add(device);
			}
			//cursor.close();
			if(devices != null && devices.size()>0){
				MyLog.print(Tag, "Field is"+key+"="+value+"There is already data in the database", MyLog.PRINT_RED);
				return true;
			}else{
				MyLog.print(Tag, "Field is"+key+"="+value+"No data in the database", MyLog.PRINT_RED);
				return false;
			}
		}else{
			return false;
		}
	}

	/**
	 * Query the device list based on the room number and user name
	 * @param context
	 * @param key
	 * @param value
	 * @param username_key
	 * @param username_value
	 * @return
	 */
	public static boolean getQueryByWhereRoomNumAndUsername(Context context,String key,String value,String username_key,String username_value){
		SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
		List<Device> devices = new ArrayList<Device>();
		if(db != null){
			Cursor cursor = db.query(Table.DEVICELIST_TABLE_NAME, null, key+"=?"+" and "+username_key+"=?", new String[]{value,username_value}, null, null, null);
			while (cursor.moveToNext()) {
				Device device = new Device();
				device.setDeviceId(cursor.getInt(cursor.getColumnIndex(Table.DEVICELIST_COLUMN_ID)));
				device.setDeviceMac(cursor.getString(cursor.getColumnIndex(Table.DEVICELIST_COLUMN_MAC)));
				device.setUsername(cursor.getString(cursor.getColumnIndex(Table.DEVICELIST_COLUMN_USERNAME)));
				device.setSipid(cursor.getString(cursor.getColumnIndex(Table.DEVICELIST_COLUMN_SIPID)));
				device.setRoomNo(cursor.getString(cursor.getColumnIndex(Table.DEVICELIST_COLUMN_ROOMNUM)));
				//device.setExtensionNum(cursor.getString(cursor.getColumnIndex(Table.DEVICELIST_COLUMN_EXTENSIONNUM)));
				device.setDeviceName(cursor.getString(cursor.getColumnIndex(Table.DEVICELIST_COLUMN_DEVICE_NAME)));
				device.setDeviceAlias(cursor.getString(cursor.getColumnIndex(Table.DEVICELIST_COLUMN_DEVICE_ALIAS)));
				device.setShieldStatus(cursor.getInt(cursor.getColumnIndex(Table.DEVICELIST_COLUMN_INCOMING_SHIELD_STATUS)));
				device.setUnitDoorNo(cursor.getString(cursor.getColumnIndex(Table.DEVICELIST_COLUMN_DOOR_DEVICE_STR)));
				devices.add(device);
			}
			cursor.close();
			if(devices != null && devices.size()>0){
				MyLog.print(Tag, "Field is"+key+"="+value+"There is already data in the database", MyLog.PRINT_RED);
//				MyLog.print(Tag, "User 11"+value+"The data"+devices.toString(), MyLog.PRINT_RED);
				return true;
			}else{
				MyLog.print(Tag, "Field is"+key+"="+value+"No data in the database", MyLog.PRINT_RED);
				return false;
			}

		}
		return false;
	}
	/**
	 * Query the device list based on the room number and user name
	 * @param context
	 * @param key
	 * @param value
	 * @param username_key
	 * @param username_value
	 * @return
	 */
	public static List<Device> getQueryByWhereRoomNumAndUsername1(Context context,String key,String value,String username_key,String username_value){
		SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
		List<Device> devices = new ArrayList<Device>();
		if(db != null){
			Cursor cursor = db.query(Table.DEVICELIST_TABLE_NAME, null, key+"=?"+" and "+username_key+"=?", new String[]{value,username_value}, null, null, null);
			while (cursor.moveToNext()) {
				Device device = new Device();
				device.setDeviceMac(cursor.getString(cursor.getColumnIndex(Table.DEVICELIST_COLUMN_MAC)));
				device.setUsername(cursor.getString(cursor.getColumnIndex(Table.DEVICELIST_COLUMN_USERNAME)));
				device.setSipid(cursor.getString(cursor.getColumnIndex(Table.DEVICELIST_COLUMN_SIPID)));
				device.setRoomNo(cursor.getString(cursor.getColumnIndex(Table.DEVICELIST_COLUMN_ROOMNUM)));
				//device.setExtensionNum(cursor.getString(cursor.getColumnIndex(Table.DEVICELIST_COLUMN_EXTENSIONNUM)));
				device.setDeviceName(cursor.getString(cursor.getColumnIndex(Table.DEVICELIST_COLUMN_DEVICE_NAME)));
				device.setDeviceAlias(cursor.getString(cursor.getColumnIndex(Table.DEVICELIST_COLUMN_DEVICE_ALIAS)));
				device.setShieldStatus(cursor.getInt(cursor.getColumnIndex(Table.DEVICELIST_COLUMN_INCOMING_SHIELD_STATUS)));
				device.setDeviceCode(cursor.getString(cursor.getColumnIndex(Table.DEVICELIST_COLUMN_DEVICECODE)));
				device.setUnitDoorNo(cursor.getString(cursor.getColumnIndex(Table.DEVICELIST_COLUMN_DOOR_DEVICE_STR)));
				devices.add(device);
			}
			return devices;
		}
		return devices;
	}
	/**
	 * Query device list based on user name
	 * @param context
	 * @param key_username
	 * @param value_username
	 * @return
	 */
	public static ArrayList<BoundResult.Device> queryByUsername(Context context,String key_username,String value_username){
		SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
		ArrayList<BoundResult.Device> devices = new ArrayList<BoundResult.Device>();
		if(db != null){
			Cursor cursor = db.query(Table.DEVICELIST_TABLE_NAME, null, key_username+"=?", new String[]{value_username}, null, null, null);
			while (cursor.moveToNext()) {
				BoundResult.Device device = new BoundResult.Device();
				device.setDeviceId(cursor.getInt(cursor.getColumnIndex(Table.DEVICELIST_COLUMN_ID)));
				device.setDeviceMac(cursor.getString(cursor.getColumnIndex(Table.DEVICELIST_COLUMN_MAC)));
				device.setUsername(cursor.getString(cursor.getColumnIndex(Table.DEVICELIST_COLUMN_USERNAME)));
				device.setSipid(cursor.getString(cursor.getColumnIndex(Table.DEVICELIST_COLUMN_SIPID)));
				device.setRoomNo(cursor.getString(cursor.getColumnIndex(Table.DEVICELIST_COLUMN_ROOMNUM)));
				//device.setExtensionNum(cursor.getString(cursor.getColumnIndex(Table.DEVICELIST_COLUMN_EXTENSIONNUM)));
				device.setDeviceName(cursor.getString(cursor.getColumnIndex(Table.DEVICELIST_COLUMN_DEVICE_NAME)));
				device.setDeviceAlias(cursor.getString(cursor.getColumnIndex(Table.DEVICELIST_COLUMN_DEVICE_ALIAS)));
				device.setShieldStatus(cursor.getInt(cursor.getColumnIndex(Table.DEVICELIST_COLUMN_INCOMING_SHIELD_STATUS)));
				device.setDeviceCode(cursor.getString(cursor.getColumnIndex(Table.DEVICELIST_COLUMN_DEVICECODE)));
				device.setUnitDoorNo(cursor.getString(cursor.getColumnIndex(Table.DEVICELIST_COLUMN_DOOR_DEVICE_STR)));
				devices.add(device);
			}
		}
//		MyLog.print(Tag, "user"+value_username+"The data"+devices.toString(), MyLog.PRINT_RED);
		return devices;
	}
	
	/**
	 * Query device list all
	 * @param context
	 * @return Device list collection
	 */
	public static List<Device> queryAll(Context context){
		SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
		List<Device> devices = new ArrayList<Device>();
		if(db != null){
			Cursor cursor = db.query(Table.DEVICELIST_TABLE_NAME, new String[]{
					Table.DEVICELIST_COLUMN_ID,
					Table.DEVICELIST_COLUMN_MAC,
					Table.DEVICELIST_COLUMN_SIPID,
					Table.DEVICELIST_COLUMN_ROOMNUM,
					Table.DEVICELIST_COLUMN_EXTENSIONNUM,
					Table.DEVICELIST_COLUMN_INCOMING_SHIELD_STATUS,
					Table.DEVICELIST_COLUMN_DEVICE_NAME,
					Table.DEVICELIST_COLUMN_DEVICE_ALIAS,
					Table.DEVICELIST_COLUMN_DOOR_DEVICE_STR,
					Table.DEVICELIST_COLUMN_DEVICECODE
			}, null, null, null, null, null);
			while (cursor.moveToNext()) {
				Device device = new Device();
				device.setDeviceId(cursor.getInt(cursor.getColumnIndex(Table.DEVICELIST_COLUMN_ID)));
				device.setDeviceMac(cursor.getString(cursor.getColumnIndex(Table.DEVICELIST_COLUMN_MAC)));
				device.setSipid(cursor.getString(cursor.getColumnIndex(Table.DEVICELIST_COLUMN_SIPID)));
				device.setRoomNo(cursor.getString(cursor.getColumnIndex(Table.DEVICELIST_COLUMN_ROOMNUM)));
				//device.setExtensionNum(cursor.getString(cursor.getColumnIndex(Table.DEVICELIST_COLUMN_EXTENSIONNUM)));
				device.setDeviceName(cursor.getString(cursor.getColumnIndex(Table.DEVICELIST_COLUMN_DEVICE_NAME)));
				device.setDeviceAlias(cursor.getString(cursor.getColumnIndex(Table.DEVICELIST_COLUMN_DEVICE_ALIAS)));
				device.setShieldStatus(cursor.getInt(cursor.getColumnIndex(Table.DEVICELIST_COLUMN_INCOMING_SHIELD_STATUS)));
				device.setDeviceCode(cursor.getString(cursor.getColumnIndex(Table.DEVICELIST_COLUMN_DEVICECODE)));
				device.setUnitDoorNo(cursor.getString(cursor.getColumnIndex(Table.DEVICELIST_COLUMN_DOOR_DEVICE_STR)));
				devices.add(device);
			}
		}
		return devices;
	}
	
	/**
	 * Delete data based on username and another field
	 * @param context Context object
	 * @param key The field to be deleted (room number)
	 * @param value The field value to be deleted (room number)
	 * @param username_key Username field
	 * @param username_value Username value
	 * @return
	 */
	public static ArrayList<BoundResult.Device> deleteDevicByRoomNumAndUsername(Context context,String key,String value,String username_key,String username_value){
		SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
		ArrayList<BoundResult.Device> devices = null;
		if(db != null){
			int row = db.delete(Table.DEVICELIST_TABLE_NAME, key+"=?"+" and "+username_key+"=?", new String[]{value,username_value});
			if(row == 0){
				MyLog.print(Tag, "failed to delete", MyLog.PRINT_RED);
			}else{
				MyLog.print(Tag, "successfully deleted", MyLog.PRINT_RED);
			}
			devices = queryByUsername(context, username_key, username_value);
		}
		return devices;
	}
	
	public static void updateDeviceNameByUserAndRoomNum(Context context,String newDeviceName,BoundResult.Device device){
		SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
		if(db != null){
			ContentValues cv = new ContentValues();
			cv.put(Table.DEVICELIST_COLUMN_DEVICE_ALIAS, newDeviceName);
//			int row = db.update(Table.DEVICELIST_TABLE_NAME, cv, Table.DEVICELIST_COLUMN_USERNAME+"=?"+" and "+Table.DEVICELIST_COLUMN_ROOMNUM+"=?",new String[]{device.getUsername(),device.getRoomNo()});
			int row = db.update(Table.DEVICELIST_TABLE_NAME, cv, Table.DEVICELIST_COLUMN_USERNAME+"=?"+" and "+Table.DEVICELIST_COLUMN_ID+"=?",new String[]{MySharedPreferenceManager.getUsername(),device.getDeviceId()+""});
//			ArrayList<BoundResult.Device> aa = queryByUsername(MyApplication.instance,Table.DEVICELIST_COLUMN_USERNAME,device.getUsername());
			db.close();
			MyLog.print(Tag, "userName:"+device.getUsername(), MyLog.PRINT_RED);
			if(row > 0){
				MyLog.print(Tag, "Data update succeeded", MyLog.PRINT_RED);
			}else{
				MyLog.print(Tag, "Data update failed", MyLog.PRINT_RED);
			}
		}
	}
	
	/**
	 * Clear data based on user
	 * @param context
	 * @param username
	 * @return
	 */
	public static boolean deleteDevicesByUser(Context context,String username){
		SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
		if(db != null){
			int row = db.delete(Table.DEVICELIST_TABLE_NAME, Table.DEVICELIST_COLUMN_USERNAME + "=?", new String[]{username});
			if(row > 0){
				MyLog.print(Tag, "Clear data successfully", MyLog.PRINT_RED);
				return true;
			}else{
				MyLog.print(Tag, "Failed to clear data", MyLog.PRINT_RED);
				return false;
			}
		}
		return false;
	}
	
	/*------------------------------------------------------------------------------------------------------------------------*/
	
	
	
}
