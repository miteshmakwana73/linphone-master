package com.quhwa.utils;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import com.quhwa.MyApplication;
import com.quhwa.activity.MainActivity;
import org.linphone.R;

public class NotifyManager {
	public static void showNotify(String title,String content){
		NotificationManager notificationManager = (NotificationManager) MyApplication.instance.getSystemService(MyApplication.instance.NOTIFICATION_SERVICE);
		NotificationCompat.Builder builder = new NotificationCompat.Builder(MyApplication.instance); 
		builder.setContentTitle(title)//Set the notification bar title
				.setContentText(content)//Set the notification bar display content
//	    .setContentIntent(getDefalutIntent(Notification.FLAG_AUTO_CANCEL)) //Set notification bar click intent
//	    .setNumber(3) //Set the number of notification collections
	    .setTicker("Qiaohua opens the door to push\n") //The notification appears for the first time in the notification bar, with a rising animation effect
				.setWhen(System.currentTimeMillis())//The time when the notification is generated will be displayed in the notification information, which is usually the time obtained by the system.
				.setPriority(Notification.PRIORITY_DEFAULT) //Set the notification priority
				.setAutoCancel(true)//Set this flag to allow notifications to be automatically cancelled when the user clicks on the panel
				.setOngoing(false)//ture，Set him as an ongoing notification. They are usually used to represent a background task, the user actively participates (such as playing music) or is waiting in some way, thus occupying the device (such as a file download, synchronization operation, active network connection)
				.setDefaults(Notification.DEFAULT_VIBRATE|NotificationCompat.DEFAULT_SOUND|NotificationCompat.DEFAULT_LIGHTS)//The easiest and most consistent way to add sound, flash, and vibration effects to notifications is to use the current user default settings, using the defaults attribute, which can be combined
				//Notification.DEFAULT_ALL  Notification.DEFAULT_SOUND Add sound // requires VIBRATE permission
	    .setSmallIcon(R.drawable.didbizlogo);//Set notifications to small ICON
		Notification notification = builder.build(); 
		int id = (int) (System.currentTimeMillis() / 1000);
		Intent intent = new Intent(MyApplication.instance, MainActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		PendingIntent pi = PendingIntent.getActivity(
           MyApplication.instance, id, intent,PendingIntent.FLAG_CANCEL_CURRENT  
        ); 
		notification.contentIntent = pi;
		notificationManager.notify(id, notification);
	}
}
