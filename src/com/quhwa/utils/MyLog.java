package com.quhwa.utils;

import android.util.Log;
import com.quhwa.MyApplication;

/**
 * Log printing class
 *
 * @author lxz
 * @date April 13, 2017
 */
public class MyLog {
	/**Font red*/
	public static final int PRINT_RED = 0;
	/**Font orange*/
	public static final int PRINT_ORANGE = 1;
	/**Font green*/
	public static final int PRINT_GREEN = 2;
	/**Font black*/
	public static final int PRINT_BLACK = 3;
	/**Font blue*/
	public static final int PRINT_BLUE = 4;
	private String Tag = "MyLog";
	/**
	 * print
	 * @param Tag Identification
	 * @param msg Print content
	 * @param printType Output font color
	 */
	public static void print(String Tag, String msg, int printType) {
		if (MyApplication.getDebug()) {
			switch (printType) {
			case PRINT_RED:
				Log.e(Tag, msg);
				break;
			case PRINT_ORANGE:
				Log.w(Tag, msg);
				break;
			case PRINT_GREEN:
				Log.i(Tag, msg);
				break;
			case PRINT_BLACK:
				Log.v(Tag, msg);
				break;
			case PRINT_BLUE:
				Log.d(Tag, msg);
				break;
			default:// PRINT_RED
				Log.e(Tag, msg);
				break;
			}
		}
	}
}
