package com.quhwa.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Preferences management class
 *
 * @author lxz
 * @date March 23, 2017
 */
public class MySharedPreferenceManager {
//	private Context context;
//	private SharedPreferences mPref;
	/**
	 * Save value to string method
	 * @param context
	 * @param tabName
	 * @param key
	 * @param value String
	 */
	public static void saveString(Context context,String tabName,String key,String value){
		SharedPreferences mPref = context.getSharedPreferences(tabName, context.MODE_PRIVATE);
		mPref.edit().putString(key, value).commit();
	}
	public static String queryString(Context context,String tabName,String key){
		SharedPreferences mPref = context.getSharedPreferences(tabName, context.MODE_PRIVATE);
		return mPref.getString(key, null);
	}
	/**
	 * Delete the value corresponding to the key in the preference table
	 * @param context
	 * @param tabName
	 * @param key
	 */
	public static void deleteData(Context context,String tabName,String key){
		SharedPreferences mPref = context.getSharedPreferences(tabName, context.MODE_PRIVATE);
		mPref.edit().remove(key);
	}
	
	/**
	 * Save value to boolean method
	 * @param context
	 * @param tabName
	 * @param key
	 * @param value
	 */
	public static void saveBoolean(Context context,String tabName,String key,boolean value){
		SharedPreferences mPref = context.getSharedPreferences(tabName, context.MODE_PRIVATE);
		mPref.edit().putBoolean(key, value).commit();
	}
	public static boolean queryBoolean(Context context,String tabName,String key){
		SharedPreferences mPref = context.getSharedPreferences(tabName, context.MODE_PRIVATE);
		return mPref.getBoolean(key, false);
	}

	public static void saveInt(Context context,String tabName,String key,int value){
		SharedPreferences mPref = context.getSharedPreferences(tabName, context.MODE_PRIVATE);
		mPref.edit().putInt(key, value).commit();
	}
	public static int queryInt(Context context,String tabName,String key){
		SharedPreferences mPref = context.getSharedPreferences(tabName, context.MODE_PRIVATE);
		return mPref.getInt(key, 0);
	}
	
	public static String getUsername(){
		return null;
		//return queryString(MyApplication.instance, Table.TAB_USER, Table.TAB_USER_NAME_KEY);
	}
}
