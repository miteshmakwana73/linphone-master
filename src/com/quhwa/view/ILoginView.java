package com.quhwa.view;

import com.quhwa.bean.UserInfo;

/**
 * Login interface
 *
 * @author lxz
 * @date March 23, 2017
 */
public interface ILoginView {
	/**
	 * Prompt to log in successfully
	 * @param userInfo
	 */
	void showToastLoginSuccess(UserInfo userInfo);
	
	/**
	 * Prompt login failed
	 */
	void showToastLoginFail();
	
	/**
	 * Login loading dialog
	 */
	void loadDialog();
	
	/**
	 * Dialog destruction
	 */
	void dismissDialog();
	
	/**
	 * Prompt input box is empty
	 */
	void showToastInputIsNull();
	
	/**
	 * Prompt login password error
	 */
	void showToastPasswordError();
	
	/**
	 * Prompt user name does not exist
	 */
	void showToastUsernameIsNotExist();
	
	/**
	 * save data
	 * @param userInfo
	 */
	void saveData(UserInfo userInfo);
	
	/**
	 * update data
	 * @param userInfo
	 */
	void updateData(UserInfo userInfo);
	
	/**
	 * Restore control display status (ie remember username and password)
	 *
	 */
	void recoverWidgetShowStatus(UserInfo userInfos);
	
}
